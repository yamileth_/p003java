package com.example.p003calculadorajava;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnIngresar;
    private Button btnCerrar;
    private EditText txtUsuario;
    private EditText txtContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrar();
            }
        });
    }

    private void iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar);
        btnCerrar = findViewById(R.id.btnCerrar);
        txtContrasena = findViewById(R.id.txtContrasena);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void ingresar() {
        String strUsuario;
        String strContra;

        strUsuario = getResources().getString(R.string.usuario);
        strContra = getResources().getString(R.string.contrasena);

        if (txtUsuario.getText().toString().equals(strUsuario) && txtContrasena.getText().toString().equals(strContra)) {
            Bundle bundle = new Bundle();
            bundle.putString("usuario", strUsuario);

            Intent intent = new Intent(MainActivity.this, activityCalculadora.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Usuario o contraseña no válidos", Toast.LENGTH_LONG).show();
        }
    }

    private void cerrar() {
        finish();
    }
}